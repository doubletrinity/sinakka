```scala
val server = SinakkaServer()

val file = """([^.]+)\.([^.]+)""".r

server.routes(route => route match {
	case GET > "hello" ?: _ => req :HttpRequest => "hello world"
	case GET > "hello" /: name ?: _ => req :HttpRequest => s"hello $name" 
	case GET > Int(a) /: "plus" /: Double(b) ?: _ => req :HttpRequest => a + b // GET {/a}/plus{/b} returns the sum of a and b iff a is an integer and b is a double
	case GET >/? queryParams if queryParams.contains("foo", "bar") => req :HttpRequest => "foo is bar" // >/? can be used to match an empty path or "/". >? can be used to match just the empty path. similarly /?: and ?: can be used to match the end of a path with an optional or no "/"
	case GET > file(name, extension) ?: _ => req :HttpRequest => s"file named $name with extension $extension" // use scala's regular expressions to match path segments and extract capture groups
	case GET > _ => httpRequest => response // httpRequest is a akka.http.scaladsl.model.HttpRequest and response can be Any, akka.http.scaladsl.model.HttpResponse, or Future[akka.http.scaladsl.model.HttpResponse]
})

server.start()
```

Implementing CORS preflight in scalakka:
```scala
case OPTIONS > _ => req :HttpRequest => {
	val headers = ListBuffer[HttpHeader]()

	for(requestedHeaders <- req.header[`Access-Control-Request-Headers`]){
		headers += `Access-Control-Allow-Headers`(requestedHeaders.headers)
	}
	for(requestedMethod <- req.header[`Access-Control-Request-Method`]){
		headers += `Access-Control-Allow-Methods`(List(requestedMethod.method))
	}

	HttpResponse(headers = headers.toList)
}
```
