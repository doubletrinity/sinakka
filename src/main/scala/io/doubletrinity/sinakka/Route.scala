package io.doubletrinity.sinakka

import akka.http.scaladsl.model.headers.{`Access-Control-Allow-Headers`, `Access-Control-Allow-Methods`, _}
import akka.http.scaladsl.model.Uri.Path.{Segment, Slash}
import akka.http.scaladsl.model.Uri.Query.Cons
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.Uri.{Path, Query}
import akka.http.scaladsl.model.HttpMethods._

import scala.collection.mutable.ListBuffer

/**
  * Created by nfischer on 12/10/2016.
  */

object Main extends App {
  val uri = Uri.apply("https://google.com/people/jim?foo=bar&a=b")
  val path = uri.path
  val tupleRoute = (GET, uri)

  //type /::: = Slash
  //type :::: = Segment

  //path match {
    //case /:::(::::("people", /:::(::::(name, Empty)))) => println(s"$name woooooooo")
    //case Slash(Segment("people", Slash(Segment(name, Path.Empty)))) => println(s"$name heeyyyyy")
  //}

  //path match {
  //  case slash /:: seg /:: rest => println(slash + " " + seg + " " + rest + 'h')
  //  case slash /:: /::(seg, rest) => println(slash + " " + seg + " " + rest + 'z')
  //}

  type Route = (HttpMethod, Uri)

  val file = """([^.]+)_(\d+)\.([^.]+)""".r

  def dooIt(route :Route) = route match {
    case OPTIONS > _ => req :HttpRequest => {
      val headers = ListBuffer[HttpHeader]()

      for(requestedHeaders <- req.header[`Access-Control-Request-Headers`]){
        headers += `Access-Control-Allow-Headers`(requestedHeaders.headers)
      }
      for(requestedMethod <- req.header[`Access-Control-Request-Method`]){
        headers += `Access-Control-Allow-Methods`(List(requestedMethod.method))
      }

      HttpResponse(headers = headers.toList)
    }
    case GET > "hello" ?: _ => req :HttpRequest => "hello world"
    case GET > "hello" /: name ?: _ => req :HttpRequest => s"hello $name"
    case GET > Int(a) /: "plus" /: Double(b) ?: _ => req :HttpRequest => a + b
    case GET >/? queryParams if queryParams.contains("foo", "bar") => req :HttpRequest => "foo is bar"
    case GET > file(name, Int(num), extension) ?: _ => req :HttpRequest => s"file $name number $num with extension $extension"
      /*
    case method >/? Derp(query :Query) => println("root")
    case method > "people" /?: query => println("more base")
    //case :>:(method, /?:("people", u)) => println("nooooooooooooo " + u)
    case method > "people" /: name /?: query => println(s"method: $method name $name query $query")
    case _ => println("oops")
    */
    case _ => req :HttpRequest => HttpResponse(StatusCodes.NotFound)
  }

  println(Uri("/hello").path)
  println(dooIt((GET, Uri("/hello")))(HttpRequest()))
  println(dooIt((GET, Uri("/hello/jim")))(HttpRequest()))
  println(dooIt((GET, Uri("/3/plus/5.5")))(HttpRequest()))
  println(dooIt((GET, Uri("/3.5/plus/5.5")))(HttpRequest()))
  println(dooIt((GET, Uri("/?foo=bar")))(HttpRequest()))
  println(dooIt((GET, Uri("?baz=bar")))(HttpRequest()))
  println(dooIt((GET, Uri("/style_5.css")))(HttpRequest()))
}

object Int {
  def unapply(s : String) : Option[Int] = try {
    Some(s.toInt)
  } catch {
    case _ : java.lang.NumberFormatException => None
  }
}

object Double {
  def unapply(s : String) : Option[Double] = try {
    Some(s.toDouble)
  } catch {
    case _ : java.lang.NumberFormatException => None
  }
}

object > {
  def unapply(arg: (HttpMethod, Uri)): Option[(HttpMethod, Uri)] = Some(arg)
}

object >/? {
  def unapply(arg: (HttpMethod, Uri)): Option[(HttpMethod, Query)] = arg._2.path match {
    case '/' /:: Path.Empty | Path.Empty => Some((arg._1, arg._2.query()))
    case _ => None
  }
}

object >? {
  def unapply(arg: (HttpMethod, Uri)): Option[(HttpMethod, Query)] = arg._2.path match {
    case Path.Empty => Some((arg._1, arg._2.query()))
    case _ => None
  }
}

object /: {
  def unapply(arg: Uri): Option[(String, Uri)] =
    arg.path match {
      case '/' /:: Derp(seg :String) /:: path => Some(seg, arg.withPath(path))
      case _ => None
  }
}

/*
object /?: {
  def unapply(uri: Uri): Option[(Uri, Query)] = uri.path match {
    case '/' /:: Path.Empty => Some(uri, uri.query())
    case Path.Empty => Some(uri, uri.query())
    case _ => None
  }

  /*
  def unapply(uri: Uri): Option[(String, Uri)] = uri.path match {
    case '/' /:: Derp(seg :String) /::Path.Empty => Some(seg, uri)
    case '/' /:: Path.Empty => Some("", uri)
    case Path.Empty => Some("", uri)
    case _ => None
  }
  */
}
*/

object ?: {
  def unapply(uri: Uri): Option[(String, Query)] = uri.path match {
    case '/' /:: Derp(seg :String) /::Path.Empty => Some(seg, uri.query())
    case _ => None
  }
}

object /?: {
  def unapply(uri: Uri): Option[(String, Query)] = uri.path match {
    case '/' /:: Derp(seg :String) /:: ('/' /:: Path.Empty | Path.Empty) => Some(seg, uri.query())
    case _ => None
  }
}

object Derp {
  def unapply[T](arg: T): Option[T] = Some(arg)
}

// unpath
object /:: {
  def unapply(arg: Path): Option[(arg.Head, Path)] = arg match {
    case Path.Empty => None
    case _ => Some(arg.head, arg.tail)
  }
}

